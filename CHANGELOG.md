# Changelog

All notable changes to this project will be documented in this file.

## Unreleased

## [1.0.0] - 2022-12-31

### Added

    - Create ESLint shareable configuration for frontend next.js projects, should work in react projects too.
