# @golemio/lint-config-frontend

Golemio Linting Shareable Config for Frontend

## Installation

```bash
yarn add -D @golemio/lint-config-frontend prettier eslint typescript@4.7.4 @typescript-eslint/parser @typescript-eslint/eslint-plugin eslint-config-next eslint-config-prettier eslint-plugin-prettier eslint-plugin-react eslint-plugin-react-hooks eslint-plugin-simple-import-sort lint-staged postcss stylelint stylelint-config-prettier-scss stylelint-config-standard-scss
```

## Usage

```javascript
// for NEXT.js projects
// .eslintrc.json
module.exports = {
    extends: "@golemio/lint-config-frontend",
};
// and copy .lintstagedrc.json, .prettierrc.json and .stylelintrc.json from repository
// optionally add to package.json scripts for local manual linting: "ts-lint": "tsc --noEmit -p tsconfig.json", "style": "stylelint **/*.scss --fix"
```

## Tests
